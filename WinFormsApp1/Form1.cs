namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b, c;
            if (double.TryParse(textBoxA.Text, out a)) double.Parse(textBoxA.Text);
            else
            {
                MessageBox.Show("������� �������� �������� A!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            if (double.TryParse(textBoxB.Text, out b)) double.Parse(textBoxB.Text);
            else
            {
                MessageBox.Show("������� �������� �������� B!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            if (double.TryParse(textBoxC.Text, out c)) double.Parse(textBoxC.Text);
            else
            {
                MessageBox.Show("������� �������� �������� C!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            double D = b * b - 4 * a * c;
            textBoxD.Text = D.ToString("F2");
            if (D == 0)
            {
                double x = (-b / (2 * a));
                textBoxX1.Text = x.ToString("F2");
                textBoxX1.Visible = true;
                label_x1.Visible = true;
            }
            else
            {
                if (D < 0)
                {
                    MessageBox.Show("����������� ����� ����! ������� ����.", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    double x1 = ((-b - Math.Sqrt(D)) / (2 * a));
                    double x2 = ((-b + Math.Sqrt(D)) / (2 * a));
                    textBoxX1.Text = x1.ToString("F2");
                    textBoxX2.Text = x2.ToString("F2");
                    textBoxX1.Visible = true;
                    label_x1.Visible = true;
                    textBoxX2.Visible = true;
                    label_x2.Visible = true;
                };
            };
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
    }
}