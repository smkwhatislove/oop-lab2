﻿using System.Text;
Console.ForegroundColor = ConsoleColor.Black;
Console.BackgroundColor = ConsoleColor.White;
Console.Clear();
//-------------------------------------------------
Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
//-------------------------------------------------
double a, b, c;
Console.WriteLine("Введіть значення змінної а: ");
a = Convert.ToDouble(Console.ReadLine());
Console.WriteLine("Введіть значення змінної b: ");
b = Convert.ToDouble(Console.ReadLine());
Console.WriteLine("Введіть значення змінної c: ");
c = Convert.ToDouble(Console.ReadLine());
//-------------------------------------------------
double d = b * b - 4 * a * c;
if (d == 0)
{
    double x = (-b / (2 * a));
    Console.WriteLine("Дискримінант дорівнює нулю. Корінь дорівнює " + x.ToString("F2") + ".");
}
else
{
    if (d < 0)
    {
        Console.WriteLine("Дискримінант менше нуля. Коренів немає");
    }
    else
    {
        double x1 = ((-b - Math.Sqrt(d)) / (2 * a));
        double x2 = ((-b + Math.Sqrt(d)) / (2 * a));
        Console.WriteLine("Дискримінант дорівнює " + d.ToString("F2") + ". Перший корінь дорівнює " + x1.ToString("F2") + ". Другий корінь дорівнює " + x2.ToString("F2") + ".");
    };
};