﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x, y, z;
            if (double.TryParse(TextBoxX.Text, out x)) double.Parse(TextBoxX.Text);
            else
            {
                MessageBox.Show("Помилка введення значення X!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            };
            if (double.TryParse(TextBoxY.Text, out y)) double.Parse(TextBoxY.Text);
            else
            {
                MessageBox.Show("Помилка введення значення Y!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            };
            if (double.TryParse(TextBoxZ.Text, out z)) double.Parse(TextBoxZ.Text);
            else
            {
                MessageBox.Show("Помилка введення значення Z!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            };
            double s = Math.Pow(2, -x) * Math.Sqrt(x + Math.Pow(Math.Abs(y), 1 / 4)) * Math.Pow(Math.Exp((x - 1) / Math.Sin(z)), 1 / 3);
            TextBoxS.Text = s.ToString("F2");
        }
    }
}
