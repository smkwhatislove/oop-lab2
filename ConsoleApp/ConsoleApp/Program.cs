﻿using System.Text;
Console.ForegroundColor = ConsoleColor.Black;
Console.BackgroundColor = ConsoleColor.White;
Console.Clear();
// ------------Зміна-кодування-на-Unicode-------------
Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
// ------Зміна-роздільника-дробової-частини-на-"."----
System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
customCulture.NumberFormat.NumberDecimalSeparator = ".";
System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
// -----------Вивід-інформації-студента---------------
Console.WriteLine("Лабораторна робота №2.");
Console.WriteLine("Виконав: Семенюк В.В, група СТ-21");
Console.WriteLine("Варіант №10");
Console.WriteLine("Завдання 1.");
// ------------Алгоритм-запису-змінних----------------
double x, y, z, s;
do
{
    Console.Write("Введіть дробове значення х = ");
    if (double.TryParse(Console.ReadLine(), out x)) break;
    else
    {
        Console.WriteLine("    Помилка введення значення x. Будь-ласка повторіть введення ще раз!!!");
    }
} while (true);
do
{
    Console.Write("Введіть дробове значення y = ");
    if (double.TryParse(Console.ReadLine(), out y)) break;
    else
    {
        Console.WriteLine("    Помилка введення значення y. Будь-ласка повторіть введення ще раз!!!");
    }
} while (true);
do
{
    Console.Write("Введіть дробове значення z = ");
    if (double.TryParse(Console.ReadLine(), out z)) break;
    else
    {
        Console.WriteLine("    Помилка введення значення z. Будь-ласка повторіть введення ще раз!!!");
    }
} while (true);
// ----------------Результат-алгоритму----------------
s = Math.Pow(2, -x) * Math.Sqrt(x + Math.Pow(Math.Abs(y), 1 / 4)) * Math.Pow(Math.Exp((x - 1) / Math.Sin(z)), 1 / 3); 
Console.WriteLine($"Результат обчислення: S = {s:F3}");