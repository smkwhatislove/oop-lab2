namespace WinFormsApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double X, Y, Z;
            if (double.TryParse(textBoxX.Text, out X)) double.Parse(textBoxX.Text);
            else
            {
                MessageBox.Show("������� �������� �������� �!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (double.TryParse(textBoxY.Text, out Y)) double.Parse(textBoxY.Text);
            else
            {
                MessageBox.Show("������� �������� �������� Y!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (double.TryParse(textBoxZ.Text, out Z)) double.Parse(textBoxZ.Text);
            else
            {
                MessageBox.Show("������� �������� �������� Z!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            double S = Math.Pow(2, -X) * Math.Sqrt(X + Math.Pow(Math.Abs(Y), 1 / 4)) * Math.Pow(Math.Exp((X - 1) / Math.Sin(Z)), 1 / 3); ;
            textBoxS.Text = S.ToString("F2");
        }
    }
}